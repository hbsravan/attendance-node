/* eslint-disable class-methods-use-this */
/* eslint no-console: ["off"], consistent-return: ["off"], max-len: ["off"] */

import mongoose from 'mongoose';
import { argv } from 'yargs';

import SystemEmail from '../app/models/systemEmails.model';
import SystemEmailVars from '../app/models/systemEmailVars.model';
import emails from './data/system_emails.json';

const { Sequelize } = require('sequelize');

const config = require('./../app/config');

const COLORS = {
  BLACK: '\x1b[30m',
  RED: '\x1b[31m',
  GREEN: '\x1b[32m',
  YELLOW: '\x1b[33m',
  BLUE: '\x1b[34m',
  MAGENTA: '\x1b[35m',
  CYAN: '\x1b[36m',
  WHITE: '\x1b[37m',
};
require('dotenv').config();

class SeedClass {
  constructor() {
    const connUri = process.env.DB_CONN_URL;
    if (connUri) {
      // mongoose.connect(
      //   connUri,
      //   {
      //     useCreateIndex: true,
      //     useNewUrlParser: true,
      //     useUnifiedTopology: true,
      //   },
      //   err => {
      //     if (err) {
      //       throw err;
      //     } else {
      console.log('Database connection successfull!');
      const dbConfig = config.mySql;

      const sequelizes = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
        host: dbConfig.host,
        dialect: dbConfig.dialect,
      });

      const { email } = argv;

      if (email) {
        const SystemEmailModel = SystemEmail.init(sequelizes, Sequelize);

        SystemEmailModel.destroy({
          where: {},
          truncate: true,
        }).then(() => {
          console.log(COLORS.YELLOW, 'Emails data cleared successfully!', COLORS.BLACK);
          emails.forEach(element => {
            const varObj = element.variables;
            delete element.variables;
            SystemEmailModel.create(element).then(isEmailAdded => {
              if (isEmailAdded && isEmailAdded.id) {
                varObj.forEach(ele => {
                  ele.templateId = isEmailAdded.id;
                });
                const SystemEmailVarsModel = SystemEmailVars.init(sequelizes, Sequelize);
                SystemEmailVarsModel.bulkCreate(varObj).then(() => {
                  console.log(COLORS.YELLOW, 'Emails seed completed successfully!', COLORS.BLACK);
                });
              }
            });
          });
        });
      }
      // }
      //   },
      // );
    }
  }
}
export default new SeedClass();
