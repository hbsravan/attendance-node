const Sequelize = require('sequelize');

module.exports = {
  s3: {
    credentials: {
      accessKeyId: process.env.S3_ACCESS_KEY,
      secretAccessKey: process.env.S3_SECRET_KEY,
    },
    bucket: process.env.S3_BUCKET,
    webUrl: process.env.S3_BUCKET_URL,
  },
  SMTP: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: process.env.SMTP_SECURE, // secure:true for port 465, secure:false for port 587
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS,
    },
  },
  seed: {
    default: false,
    capabilities: false,
    regions: false,
    settings: false,
    systemEmail: false,
    page: false,
  },
  uploadPath: {
    products: 'express/products/',
    categories: 'express/categories/',
    options: 'express/options/',
    banners: 'express/banners/',
  },
  changePasswordPath: '/reset-password/',
  setPasswordPath: '/change_password/',
  logo: '/assets/images/logo.png',
  mySql: {
    username: process.env.MY_SQL_USERNAME,
    password: process.env.MY_SQL_PASSWORD,
    database: process.env.MY_SQL_DB,
    host: process.env.MY_SQL_HOST,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    dialectOptions: {},
    operatorsAliases: Sequelize.Op,
    logging: false,
  },
  whiteListAPIS: ['/admin/users/capabilities'],
};
