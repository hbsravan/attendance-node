import express from 'express';
import compression from 'compression';
import path from 'path';
import bodyParser from 'body-parser';
// import Sequelize from 'sequelize';
import helmet from 'helmet';
import cors from 'cors';

import routes from './routes';

const models = require('./models');

const app = express();

class ExpressApp {
  constructor() {
    models.default.sequelize.sync({ force: false }).then(() => {
      console.log('Database & tables created!');
      app.emit('ready');
    });

    this.setMiddleware();
    this.helmetSecurity();
    this.setRoutes();
    return app;
  }

  setMiddleware() {
    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');
    app.use('/public', express.static(path.join(__dirname, 'public')));
    app.disable('x-powered-by');

    app.use(
      bodyParser.urlencoded({
        extended: false,
      }),
    );
    app.use(bodyParser.json());
    app.use(compression());
    app.use(cors());
  }

  helmetSecurity() {
    const SIX_MONTHS = 15778476000;
    app.use(helmet.hidePoweredBy());
    app.use(helmet.frameguard());
    app.use(helmet.xssFilter());
    app.use(helmet.noSniff());
    app.use(helmet.ieNoOpen());
    app.use(
      helmet.hsts({
        maxAge: SIX_MONTHS,
        includeSubDomains: true,
        force: true,
      }),
    );
  }

  setRoutes() {
    app.use('/', routes);
  }
}

export default new ExpressApp();
