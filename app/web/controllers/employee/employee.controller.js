import httpStatus from 'http-status';
import { matchedData } from 'express-validator';
import employeeService from '../../services/employee.service';
import BaseController from '../base.controller';
import objectService from '../../services/object.service';

class EmployeeController extends BaseController {
  constructor() {
    super('Employee', ['firstName', 'lastName', 'email', 'created']);
  }

  update = async req => {
    const data = matchedData(req);
    const employee = await employeeService.model.findOne({
      where: { id: req.params.id },
    });
    if (employee) {
      employee.firstName = data.firstName;
      employee.lastName = data.lastName;
      employee.email = data.email;
      employee.gender = data.gender;
      employee.country = data.country;
      employee.state = data.state;
      employee.city = data.city;
      employee.active = data.active;
      employee.birthdate = data.birthdate;
      employee.mobile = data.mobile;
      employee.updatedBy = req.user.id;
      await employee.save();
      return this.sendResponse(httpStatus.OK, true, this.messages.UPDATE_SUCCESS);
    }
    return this.sendResponse(httpStatus.OK, false, this.messages.NOT_FOUND);
  };

  getById = async req => {
    const employee = await employeeService.model.findOne({
      where: { id: req.user.id },
    });
    if (employee) {
      return this.sendResponse(
        httpStatus.OK,
        true,
        this.messages.DETAILS_SUCCESS,
        objectService.getCustDetailObject(employee),
      );
    }
    return this.sendResponse(httpStatus.OK, false, this.messages.NOT_FOUND);
  };

  changePassword = async req => {
    const data = req.body;
    try {
      const user = await employeeService.model.findOne({
        where: {
          id: data.id,
        },
        attributes: ['id', 'firstName', 'lastName', 'email', 'mobile', 'passwordHash'],
      });

      if (!user) {
        return this.sendResponse(httpStatus.UNAUTHORIZED, false, 'User not found!');
      }
      if (!user.authenticate(req.body.oldPassword, user.passwordHash)) {
        return this.sendResponse(httpStatus.OK, false, 'Your old password does not match');
      }

      user.passwordHash = data.newPassword;
      await user.save();

      return this.sendResponse(httpStatus.OK, false, 'Password changed successfully.');
    } catch (ex) {
      console.log(ex);
      return this.sendResponse(httpStatus.OK, false, 'Error occured while changing password.');
    }
  };
}
export default EmployeeController;
