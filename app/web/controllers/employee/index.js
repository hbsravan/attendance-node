import { body, checkSchema } from 'express-validator';
import express from 'express';
import EmployeeController from './employee.controller';
import { controllerHandler } from '../../../utils/async.handler';
import validator from './employee.validation';
import auth from '../../../services/auth.service';

const controller = new EmployeeController();
const router = express.Router();

class EmployeeRoutes {
  static route() {
    router.get(
      '/detail',
      controllerHandler(controller.getById, req => [req]),
    );

    router.post(
      '/change-password',
      [
        body('id')
          .isInt()
          .withMessage('Id is required and it should mongoId.'),
        body('oldPassword')
          .not()
          .isEmpty()
          .withMessage('oldPassword is required.'),
        body('newPassword')
          .not()
          .isEmpty()
          .withMessage('newPassword is required.'),
      ],
      controllerHandler(controller.changePassword, req => [req]),
    );

    return router;
  }
}
export default EmployeeRoutes.route();
