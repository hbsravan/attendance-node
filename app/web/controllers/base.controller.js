import { EventEmitter } from 'events';

import MessageService from '../../services/message.service';

import utils from '../../utils';

class BaseController extends EventEmitter {
  constructor(entity, sortField) {
    super();
    this.messages = new MessageService(entity);

    this.sortField = sortField || [];
  }

  sendResponse = (status, success, message, result) => ({
    status,
    success,
    data: result,
    message,
  });

  listOptions = data => {
    const options = {
      filters: data.filters || [],
      keyword: data.keyword || '',
      startDate: data.startDate || '',
      endDate: data.endDate || '',
      offset: parseInt(data.offset, 10) || 0,
      limit: parseInt(data.limit, 10) || 10,
      page: parseInt(data.page, 10) || 1,
    };
    if (!options.offset && options.page > 1) {
      options.offset = (options.page - 1) * options.limit;
    }
    options.keyword = utils.escape(options.keyword.trim());
    const sortKeys = data.sort ? Object.keys(data.sort) : [];
    let sort = {
      created: 'desc',
    };
    if (sortKeys.length && this.sortField.indexOf(sortKeys[0]) > -1) {
      sort = {};
      sort[sortKeys] = data.sort[sortKeys];
      options.sort = sort;
    }

    return options;
  };

  getPagination = (entity, limit, currentPage) => {
    if (entity.rows) {
      const doc = {
        docs: entity.rows,
        total: entity.count,
        page: currentPage,
        totalPages: Math.ceil(entity.count / limit),
        next: Math.ceil(entity.count / limit) > currentPage,
        nextPage: Math.ceil(entity.count / limit) > currentPage ? (currentPage += 1) : 0,
      };
      return doc;
    }
    return {
      docs: [],
      total: 0,
      page: 0,
      offset: 0,
      next: false,
    };
  };
}

export default BaseController;
