import { matchedData } from 'express-validator';

import BaseController from './controllers/base.controller';
import EmployeeService from './services/employee.service';
import TokenService from '../services/token.service';
import SpeakEasyService from './services/speakeasy.service';
import utils, { status } from '../utils';
import EmailService from '../utils/email.service';

class AuthController extends BaseController {
  constructor() {
    super('Employee');
  }

  create = async req => {
    const employee = matchedData(req);
    const exist = await EmployeeService.findOne({
      where: { email: employee.email },
    });
    if (exist) {
      return this.sendResponse(status.OK, false, 'Employee already exists with the provided email');
    }
    const result = await EmployeeService.create(employee, req);
    if (result) {
      return this.sendResponse(status.OK, true, this.messages.INSERT_SUCCESS, {
        employeeCode: result.employeeCode,
        id: result.id,
      });
    }
    return this.sendResponse(status.OK, false, this.messages.INSERT_ERROR);
  };

  login = async req => {
    let tokenData = null;
    const data = matchedData(req);
    const user = await EmployeeService.login(data);
    if (user) {
      if (data.password && user.passwordHash) {
        if (await user.authenticate(data.password, user.passwordHash)) {
          if (user.twoFactorEnabled) {
            if (!user.secret) {
              user.secret = SpeakEasyService.generateSecret();
              await user.save();
            }
            const otp = SpeakEasyService.generateOtpFromSecret(user.secret);
            if (!otp) {
              return this.sendResponse(
                status.OK,
                false,
                'There is some problem in generating OTP, please try again in sometime',
                {},
              );
            }
            const variables = utils.replaceCompanyVariables(req);
            variables.push({
              item: 'otp',
              value: otp,
            });
            await EmailService(user.email, 'SEND_OTP', variables);
            return this.sendResponse(status.OK, true, 'Please find OTP from the mail.', {
              twoFactorEnabled: user.twoFactorEnabled,
              employeeCode: user.employeeCode,
              type: user.type,
            });
          }

          tokenData = await TokenService.getToken(user);
          return this.sendResponse(status.OK, true, this.messages.LOGGED_IN_SUCCESS, {
            token: tokenData.token,
            twoFactorEnabled: user.twoFactorEnabled,
            employeeCode: user.employeeCode,
            type: user.type,
          });
        }
        return this.sendResponse(status.NOT_FOUND, false, this.messages.INVALID_LOGIN, {});
      }
      return this.sendResponse(status.NOT_FOUND, false, 'Please provide password.', {});
    }
    return this.sendResponse(status.NOT_FOUND, false, 'User not found.', {});
  };

  verifyOtp = async req => {
    let tokenData = null;
    const data = matchedData(req);
    const user = await EmployeeService.login(data);
    if (user) {
      const veifyOtp = SpeakEasyService.verifyOtp(user.secret, data.otp);
      if (!veifyOtp) {
        return this.sendResponse(status.OK, false, 'OTP verification failed', {});
      }
      tokenData = await TokenService.getToken(user);
      return this.sendResponse(status.OK, true, this.messages.LOGGED_IN_SUCCESS, {
        token: tokenData.token,
      });
    }
    return this.sendResponse(status.OK, false, 'Invalid username or password', {});
  };

  verifyApproveToken = async req => {
    const data = matchedData(req);
    const user = await EmployeeService.model.findOne({
      where: {
        _id: data.id,
        resetVerificationCode: data.code,
      },
    });
    if (user) {
      const myDate = new Date(user.resetTokenExpires);
      if (myDate.getMilliseconds() > new Date().getMilliseconds()) {
        return this.sendResponse(status.OK, true, 'User successfully verified.', []);
      }
    }
    return this.sendResponse(status.OK, false, 'Account activation link is not valid.');
  };

  setPassword = async req => {
    const data = matchedData(req);
    const user = await EmployeeService.setPassword(data);
    if (user) {
      return this.sendResponse(status.OK, true, 'Password set successfully.', {});
    }
    return this.sendResponse(status.OK, false, 'Error occured while password update.');
  };

  verifyToken = async req => {
    const data = matchedData(req);
    const where = {
      id: data.id,
      resetVerificationCode: data.code,
    };
    const doc = await EmployeeService.verifyToken(where);

    if (doc.tokenExpired) {
      return this.sendResponse(status.OK, false, 'Token is expired, Please trye again.', []);
    }
    if (doc.tokenVerified) {
      return this.sendResponse(status.OK, true, 'Token verified successfully.', []);
    }
    return this.sendResponse(
      status.OK,
      false,
      'Verification token is not valid, please retry forgot password!',
      [],
    );
  };

  forgotPassword = async req => {
    const result = await EmployeeService.forgotPassword(req);
    if (result && result.id) {
      return this.sendResponse(
        status.OK,
        true,
        'Email has been sent to your registered email address to set password.',
        result,
      );
    }
    return this.sendResponse(status.OK, false, 'Email not found!');
  };
}
export default AuthController;
