import express from 'express';
import { body, checkSchema } from 'express-validator';
import AuthController from './auth.controller';
import EmployeeController from './controllers/employee/employee.controller';
import { controllerHandler } from '../utils/async.handler';
import auth from '../services/auth.service';

import EmployeeRoutes from './controllers/employee';
import defaultLimiter from '../services/rate-limiter.service';
import validator from './auth.validation';

const router = express.Router();
const authController = new AuthController();
const employeeController = new EmployeeController();

class WebRoutes {
  static routes() {
    router.post(
      '/signup',
      checkSchema(validator.create()),
      controllerHandler(authController.create, req => [req]),
    );

    router.post(
      '/verify-token',
      [
        body('id')
          .isInt()
          .withMessage('Id is required and it should mongoId.'),
        body('code')
          .not()
          .isEmpty()
          .withMessage('code is required to match'),
      ],
      controllerHandler(employeeController.verifyToken, req => [req]),
    );

    router.post(
      '/set-password',
      [
        body('id')
          .isInt()
          .withMessage('Id is required.'),
        body('password')
          .not()
          .isEmpty()
          .withMessage('password is required.'),
        body('code')
          .not()
          .isEmpty()
          .withMessage('code is required to match'),
      ],
      controllerHandler(authController.setPassword, req => [req]),
    );

    router.post(
      '/login',
      defaultLimiter,
      [
        body('email')
          .isLength({ min: 1 })
          .withMessage('Email is required'),
        body('password')
          .isLength({ min: 1 })
          .withMessage('Password is required'),
        body('type')
          .isLength({ min: 1 })
          .withMessage('Type is required.')
          .isIn(['HB', 'TR', 'VI'])
          .withMessage(),
      ],
      controllerHandler(authController.login, req => [req]),
    );

    router.post(
      '/verify-otp',
      [
        body('email')
          .isLength({ min: 1 })
          .withMessage('Email is required'),
        body('otp')
          .isLength({ min: 1 })
          .withMessage('otp is required'),
        body('type')
          .isLength({ min: 1 })
          .withMessage('Type is required.')
          .isIn(['HB', 'TR', 'VI'])
          .withMessage(),
      ],
      controllerHandler(authController.verifyOtp, req => [req]),
    );

    router.post(
      '/forgot-password',
      defaultLimiter,
      [
        body('email')
          .isLength({ min: 1, max: 255 })
          .withMessage('email is required')
          .isEmail()
          .withMessage('Please enter valid email.')
          .trim()
          .escape(),
      ],
      controllerHandler(authController.forgotPassword, req => [req]),
    );

    router.use('/employees', auth.checkWebLogin(), EmployeeRoutes);

    return router;
  }
}
export default WebRoutes.routes();
