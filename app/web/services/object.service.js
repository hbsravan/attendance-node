/**
 * Get user object to return in response
 * @param {Object} item User detail to transform
 *
 * @return User detail object
 */
class ObjectService {
  getCustDetailObject = item => {
    const doc = {
      id: item.id,
      firstName: item.firstName,
      lastName: item.lastName,
      email: item.email,
      type: item.type,
      twoFactorEnabled: item.twoFactorEnabled,
      employeeCode: item.employeeCode,
    };
    return doc;
  };
}

export default new ObjectService();
