import { Op } from 'sequelize';
import moment from 'moment';
import Employee from '../../models/employee.model';
import BaseService from '../../services/base.service';
import utils from '../../utils';
import config from '../../config';
import EmailService from '../../utils/email.service';

class EmployeeService extends BaseService {
  constructor() {
    super(Employee);
  }

  create = async (cust, req) => {
    const verificationCode = utils.GenerateCode();
    const date = new Date();
    const tomorrowDate = new Date(date.getTime() + 24 * 60 * 60 * 1000);
    const employee = {};
    employee.firstName = cust.firstName;
    employee.lastName = cust.lastName;
    employee.email = cust.email;
    employee.gender = cust.gender;
    employee.active = cust.active;
    employee.birthdate = cust.birthdate;
    employee.mobile = cust.mobile;
    employee.type = cust.type;
    employee.country = cust.country;
    employee.twoFactorEnabled = cust.factory_authentication;
    employee.state = cust.state;
    employee.city = cust.city;
    employee.employeeCode = await this.generateCode(cust);
    employee.resetTokenExpires = tomorrowDate;
    employee.resetVerificationCode = verificationCode;
    employee.isEmailVerified = true;
    employee.passwordHash = cust.passwordHash;
    const custs = await Employee.create(employee);

    if (custs) {
      const verificationLink = `${utils.getSiteSetting(req, 'COMPANY_WEBSITE')
        + config.setPasswordPath
        + custs.resetVerificationCode}/${custs.id}`;
      const variables = utils.replaceCompanyVariables(req);
      variables.push(
        { item: 'firstName', value: custs.firstName },
        { item: 'lastName', value: custs.lastName },
        { item: 'employeeCode', value: `${custs.type}-${custs.employeeCode}` },
        { item: 'verificationLink', value: verificationLink },
      );
      await EmailService(custs.email, 'EMPLOYEE_REGISTRATION', variables);
      return custs;
    }
    return false;
  };

  changeStatus = async data => Employee.update({ active: data.active }, { where: { id: data.ids } });

  getByCondition = options => Employee.paginate(options);

  list = async options => {
    const AND = [{ deleted: false }];
    const selectFields = [
      'id',
      'firstName',
      'lastName',
      'email',
      'gender',
      'active',
      'birthdate',
      'mobile',
      'country',
      'state',
      'city',
    ];

    if (options.filters.length > 0) {
      options.filters.forEach(filter => {
        if (filter.key === 'name' && filter.value) {
          AND.push({
            [Op.or]: [
              {
                firstName: { [Op.like]: `%${filter.value}%` },
              },
              {
                lastName: { [Op.like]: `%${filter.value}%` },
              },
              {
                email: { [Op.like]: `%${filter.value}%` },
              },
            ],
          });
        }
      });
    }

    if (options.keyword) {
      AND.push({
        [Op.or]: [
          { firstName: { [Op.like]: `%${options.keyword}%` } },
          { lastName: { [Op.like]: `%${options.keyword}%` } },
          { email: { [Op.like]: `%${options.keyword}%` } },
        ],
      });
    }

    const result = await this.findWithPagination({
      where: { [Op.and]: AND },
      attributes: selectFields,
      limit: options.limit,
      offset: options.offset,
    });
    return result;
  };

  generateCode = async cust => {
    let code = 0;
    const student = await this.findOne({
      limit: 1,
      where: { type: cust.type },
      attributes: ['employeeCode'],
      order: [['employeeCode', 'DESC']],
    });
    if (student && student.employeeCode) {
      code = parseInt(student.employeeCode, 10);
    }
    const codeVal = utils.rjust((code + 1).toString(), 4, '0');
    const sCode = `${codeVal}`;
    return sCode;
  };

  login = async data => {
    const cust = await this.findOne({
      where: { employeeCode: data.email, type: data.type },
      attributes: [
        'id',
        'firstName',
        'lastName',
        'employeeCode',
        'email',
        'mobile',
        'profile_pic',
        'resetVerificationCode',
        'twoFactorEnabled',
        'type',
        'secret',
        'passwordHash',
      ],
    });
    return cust;
  };

  verifyToken = async data => {
    const employee = await this.findOne({ where: data });

    if (employee) {
      const myDate = new Date(employee.resetTokenExpires);
      if (myDate > new Date()) {
        employee.isEmailVerified = true;
        await employee.save();
        return { tokenVerified: true };
      }
      return { tokenExpired: true };
    }
    return { userNotFound: true };
  };

  setPasswordOld = async data => {
    const password = utils.getPasswordHash(data.password);
    const doc = await Employee.update(
      {
        passwordHash: password,
        resetVerificationCode: '',
        resetTokenExpires: null,
        oldPasswords: [password],
      },
      {
        where: {
          id: data.id,
        },
      },
    );

    if (doc && doc.length && doc[0] > 0) {
      return true;
    }

    return null;
  };

  setPassword = async data => {
    // const password = utils.getPasswordHash(data.password);

    const user = await Employee.findOne({
      where: {
        id: data.id,
        resetVerificationCode: data.code,
      },
    });
    if (user) {
      const currentTime = moment();
      const tokenExpiryTime = moment(user.resetTokenExpires);
      if (currentTime > tokenExpiryTime) {
        // return this.sendResponse(200, false, 'Password token is already expired.');
        return null;
        // return this.throwError(200, false, 'Password token is already expired.');
      }
      user.passwordHash = data.password;
      user.resetVerificationCode = '';
      user.resetTokenExpires = null;
      await user.save();

      if (user) {
        return true;
      }
      return null;
    }
    return null;
    // return this.throwError(200, false, 'Password token is already expired.');
  };

  forgotPassword = async req => {
    const data = req.body;
    const verificationCode = utils.GenerateCode();
    const date = new Date();
    const tomorrowDate = new Date(date.getTime() + 24 * 60 * 60 * 1000);
    const result = await Employee.findOne({
      where: { email: data.email },
    });
    if (result) {
      result.resetVerificationCode = verificationCode;
      result.resetTokenExpires = tomorrowDate;
      await result.save();
      const verificationLink = `https://8cdecf97.ngrok.io/${config.setPasswordPath
        + verificationCode}/${result.id}`;
      const variables = utils.replaceCompanyVariables(req);
      variables.push(
        {
          item: 'firstName',
          value: result.firstName,
        },
        {
          item: 'lastName',
          value: result.lastName,
        },
        {
          item: 'verificationLink',
          value: verificationLink,
        },
      );
      await EmailService(data.email, 'EMPLOYEE_FORGOT_PASSWORD', variables);
      return { id: result.id };
    }
    return false;
  };
}
export default new EmployeeService();
