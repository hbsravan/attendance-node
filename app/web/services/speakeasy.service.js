const speakeasy = require('speakeasy');

const methods = {};
methods.generateSecret = () => {
  const secret = speakeasy.generateSecret({ length: 20 });
  return secret.base32;
};

methods.generateOtpFromSecret = secret => {
  const token = speakeasy.totp({
    secret,
    encoding: 'base32',
  });
  return token;
};

methods.verifyOtp = (secret, token) => {
  const tokenValidates = speakeasy.totp.verify({
    secret,
    encoding: 'base32',
    token,
    window: 1,
  });
  return tokenValidates;
};

module.exports = methods;
