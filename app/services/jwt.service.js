import jwt from 'jsonwebtoken';

import utils from '../utils';

const jwtSecret = process.env.JWT_SECRET;

class JwtService {
  checkJwtError = (err, req, res, next) => {
    if (err) {
      if (err.name === 'UnauthorizedError') {
        return utils.errorWithProperty(
          res,
          'You are not authorized to access the requested resource.',
          { code: 401 },
          401,
        );
      }
    }
    return next();
  };

  /**
   * Returns a jwt token, signed by the app secret
   *
   * @param {object|buffer|string} payload Payload to create token with
   * @param {object} opts options to create token with
   */
  createJwtToken = function(payload, opts) {
    return jwt.sign(payload, jwtSecret, opts);
  };
}
export default new JwtService();
