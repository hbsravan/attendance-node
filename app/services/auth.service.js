/* eslint consistent-return: ["off"], prefer-destructuring: ["off"] */

import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';
import compose from 'composable-middleware';

import utils, { status } from '../utils';
import BaseService from './base.service';
import User from '../models/user.model';
import Employee from '../models/employee.model';
import Enum from '../utils/enums';

const jwtSecret = process.env.JWT_SECRET;

const validateJwt = expressJwt({
  secret: jwtSecret,
});
const unauthorizedError = {
  success: false,
  message: 'You are not authorized to perform this operation.',
  data: {
    code: 401,
  },
};

class AuthService extends BaseService {
  /**
   * Attach the user object to the request if authenticated
   * Otherwise returns 401
   */
  isAuthenticated = () => compose()
    .use(validateJwt)
    .use((err, req, res, next) => {
      if (err) {
        // @todo Uncomment below if next(err) is handled
        // if (err.name === 'UnauthorizedError') {
        //   const error = new Error('Missing or invalid token.');
        //   error.code = 401;
        //   next(error);
        // }

        // @todo Remove below code if above next(err) is handled
        return res.status(status.UNAUTHORIZED).json({
          success: false,
          message: 'You are not authorized to perform this operation.',
        });
      }
      next();
    })
    .use((req, res, next) => {
      let token = null;
      if (
        req.headers.authorization
          && req.headers.authorization.split(' ')[0].toLowerCase() === 'bearer'
      ) {
        token = req.headers.authorization.split(' ')[1];
      }

      Employee.findById(req.user.id)
        .select('email token active firstName lastName profile_pic dailCode mobile')
        .then(user => {
          if (!user) {
            return res.send(401);
          }
          if (user.token !== token) {
            return utils.errorWithProperty(res, 'Your token has expired.', { code: 401 }, 401);
          }

          req.user = user;
          next();
          return user;
        });
    });

  /**
   * Attach the user object to the request if user is valid and authorized
   * Otherwise returns error
   */
  checkAdminLogin = () => compose()
    .use((req, res, next) => {
      if (req.headers && !req.headers.authorization) {
        return res.status(status.UNAUTHORIZED).send(unauthorizedError);
      }
      const parts = req.headers.authorization.split(' ');
      let authorizationToken;
      if (parts.length === 2) {
        const scheme = parts[0];
        const credentials = parts[1];
        if (/^Bearer$/i.test(scheme)) {
          authorizationToken = credentials;
          jwt.verify(authorizationToken, jwtSecret, (err, token) => {
            if (err) {
              return res.status(status.UNAUTHORIZED).send(unauthorizedError);
            }
            req.user = token;
            next();
          });
        } else {
          return res.status(status.UNAUTHORIZED).send(unauthorizedError);
        }
      } else {
        return res.status(status.UNAUTHORIZED).send(unauthorizedError);
      }
    })
    .use((req, res, next) => User.findOne({
      where: {
        id: req.user.id,
        roleId: req.user.roleId,
        status: 'Active',
      },
      attributes: [
        'id',
        'firstName',
        'lastName',
        'email',
        'roleId',
        'profile_pic',
        'status',
        'mobile',
      ],
    }).then(user => {
      next();
      return null;
    }));

  checkWebLogin = () => compose()
    .use((req, res, next) => {
      if (req.headers && !req.headers.authorization) {
        return res.status(status.UNAUTHORIZED).send(unauthorizedError);
      }
      const parts = req.headers.authorization.split(' ');
      let authorizationToken;
      if (parts.length === 2) {
        const scheme = parts[0];
        const credentials = parts[1];
        if (/^Bearer$/i.test(scheme)) {
          authorizationToken = credentials;
          jwt.verify(authorizationToken, jwtSecret, (err, token) => {
            if (err) {
              return res.status(status.UNAUTHORIZED).send(unauthorizedError);
            }
            req.user = token;
            next();
          });
        } else {
          return res.status(status.UNAUTHORIZED).send(unauthorizedError);
        }
      } else {
        return res.status(status.UNAUTHORIZED).send(unauthorizedError);
      }
    })
    .use((req, res, next) => Employee.findOne({
      where: {
        id: req.user.id,
      },
      attributes: [
        'id',
        'firstName',
        'lastName',
        'email',
        'profile_pic',
        'mobile',
        'employeeCode',
        'type',
      ],
    }).then(user => {
      if (!user) {
        return utils.errorWithProperty(
          res,
          'You do not have permission to perform this action.',
        );
      }
      next();
      return null;
    }));

  isAuthorizedGetRole = function() {
    return compose()
      .use(validateJwt)
      .use((err, req, res, next) => {
        next();
      })
      .use((req, res, next) => {
        if (req.user !== undefined) {
          User.findById(req.user.id, 'name email roleId active')
            .populate('roleId', 'slug')
            .then(user => {
              if (!user) {
                return utils.errorWithProperty(res, 'User not found.');
              }
              req.user = user;
              next();
              return user;
            })
            .catch(utils.handleError(res, next));
        } else {
          next();
        }
      });
  };
}

export default new AuthService();
