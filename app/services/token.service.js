import jwtService from './jwt.service';
import config from '../config';

class TokenService {
  getToken = async user => {
    const tokenData = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      employeeCode: user.employeeCode,
      email: user.email,
      roleId: user.roleId,
      resetVerificationCode: user.resetVerificationCode ? user.resetVerificationCode : null,
      status: user.status ? user.status : null,
      // profileImg:
      // 	user.profile_pic && user.profile_pic.length > 0
      // 		? config.awsWebUrl + config.uploadPath.user + user.profile_pic[0]
      // 		: '',
    };
    const createdToken = jwtService.createJwtToken(tokenData, {
      expiresIn: '2h',
    });
    const returnData = {
      token: createdToken,
    };
    return returnData;
  };
}

export default new TokenService();
