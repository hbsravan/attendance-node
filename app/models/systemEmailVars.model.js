import Sequelize from 'sequelize';

class SystemEmailVars extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init({
      name: {
        type: DataTypes.STRING(100),
      },
      description: {
        type: DataTypes.STRING(500),
      },
      templateId: {
        type: DataTypes.INTEGER,
      },
    }, {
      timestamps: true,
      tableName: 'systemEmailsVars',
      sequelize,
      indexes: [
        {
          name: 'templateId_index',
          method: 'BTREE',
          fields: ['templateId'],
        },
      ],
    });
  }

  static associations(models) {
    this.belongsTo(models.SystemEmail, {
      constraints: false,
      as: 'variables',
      foreignKey: {
        name: 'templateId',
        allowNull: false,
      },
    });
  }
}


export default SystemEmailVars;
