import Sequelize from 'sequelize';
import config from '../config';

import EmployeeModel from './employee.model';
import SystemEmailsModel from './systemEmails.model';
import SystemEmailVarsModel from './systemEmailVars.model';
import UserModel from './user.model';

const dbConfig = config.mySql;
const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect,
});

const models = {
  Employee: EmployeeModel.init(sequelize, Sequelize),
  SystemEmails: SystemEmailsModel.init(sequelize, Sequelize),
  SystemEmailVars: SystemEmailVarsModel.init(sequelize, Sequelize),
  User: UserModel.init(sequelize, Sequelize),
};

Object.values(models)
  .filter(model => typeof model.associate === 'function')
  .forEach(model => {
    model.associate(models);
  });

const db = {
  ...models,
  sequelize,
};
export default db;
