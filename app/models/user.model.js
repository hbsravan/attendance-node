import bcrypt from 'bcrypt';
import Sequelize from 'sequelize';

class User extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    const createPasswordHash = async instance => {
      if (instance.changed('passwordHash')) {
        const hash = await bcrypt.hash(instance.get('passwordHash'), 10);
        instance.set('passwordHash', hash);
        return true;
      }
      return true;
    };
    return super.init(
      {
        firstName: {
          type: DataTypes.STRING(50),
          allowNull: false,
        },
        lastName: {
          type: DataTypes.STRING(50),
          allowNull: false,
        },
        roleId: {
          type: DataTypes.TEXT,
        },
        email: {
          type: DataTypes.STRING,
          unique: {
            msg: 'User already exists with the provided email.',
          },
        },
        about: {
          type: DataTypes.TEXT,
        },
        gender: {
          type: DataTypes.ENUM('male', 'female'),
        },
        mobile: {
          type: DataTypes.STRING(20),
        },
        lastlogin: {
          type: DataTypes.DATE,
        },
        resetVerificationCode: {
          type: DataTypes.STRING,
        },
        ipAddress: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        status: {
          type: DataTypes.ENUM('Pending', 'Active', 'In Active'),
          defaultValue: 'Pending',
        },
        deleted: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        profile_pic: {
          type: DataTypes.STRING,
          defaultValue: '',
        },

        resetToken: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        resetTokenExpires: {
          type: DataTypes.DATE,
        },
        passwordHash: {
          type: DataTypes.STRING,
        },
        oldPasswords: {
          type: DataTypes.STRING,
        },

        token: {
          type: DataTypes.STRING,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        updatedBy: {
          type: DataTypes.INTEGER,
        },
      },
      {
        tableName: 'users',
        sequelize,
        hooks: {
          beforeCreate: createPasswordHash,
          beforeUpdate: createPasswordHash,
        },
      },
    );
  }

  static associate(models) {}
}

export default User;
User.prototype.authenticate = async (plainPassword, password) => {
  const match = await bcrypt.compare(plainPassword, password);
  if (match) {
    return true;
  }
  return false;
};
