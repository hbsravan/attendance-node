import Sequelize from 'sequelize';
import bcrypt from 'bcrypt';

class Employee extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    const createPasswordHash = async instance => {
      if (instance.changed('passwordHash')) {
        try {
          const hash = await bcrypt.hash(instance.get('passwordHash'), 10);
          instance.set('passwordHash', hash);
          return true;
        } catch (err) {
          return err;
        }
      }
      return true;
    };
    return super.init(
      {
        firstName: {
          type: DataTypes.STRING(50),
          allowNull: false,
        },
        lastName: {
          type: DataTypes.STRING(50),
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          unique: {
            msg: 'Employee already exists with the provided email.',
          },
        },
        gender: {
          type: DataTypes.ENUM('male', 'female'),
          allowNull: true,
        },
        type: {
          type: DataTypes.ENUM('HB', 'TR', 'VI'),
          defaultValue: 'HB',
        },
        birthdate: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        age: {
          type: DataTypes.INTEGER,
          defaultValue: 0,
        },
        country: {
          type: DataTypes.INTEGER,
        },
        state: {
          type: DataTypes.INTEGER,
        },
        city: {
          type: DataTypes.INTEGER,
        },
        dailCode: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        employeeCode: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        secret: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        twoFactorEnabled: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        mobile: {
          type: DataTypes.STRING(20),
          defaultValue: '',
        },
        otp: {
          type: DataTypes.INTEGER,
          defaultValue: 0,
        },
        lastlogin: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        deviceId: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        deviceType: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        deviceToken: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        isEmailVerified: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        receiveNotifications: {
          type: DataTypes.BOOLEAN,
          defaultValue: true,
        },
        resetVerificationCode: {
          type: DataTypes.STRING,
        },
        ipAddress: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        facebookId: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        twitterId: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        instagramId: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        googleId: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        active: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        deleted: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        profile_pic: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        resetToken: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        resetTokenExpires: {
          type: DataTypes.DATE,
        },
        passwordHash: {
          type: DataTypes.STRING,
        },
        token: {
          type: DataTypes.STRING,
          defaultValue: '',
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        updatedBy: {
          type: DataTypes.INTEGER,
          defaultValue: 0,
        },
      },
      {
        tableName: 'employees',
        sequelize,
        hooks: {
          beforeCreate: createPasswordHash,
          beforeUpdate: createPasswordHash,
        },
        indexes: [
          {
            name: 'country_index',
            method: 'BTREE',
            fields: ['country'],
          },
          {
            name: 'state_index',
            method: 'BTREE',
            fields: ['state'],
          },
          {
            name: 'city_index',
            method: 'BTREE',
            fields: ['city'],
          },
        ],
      },
    );
  }

  static associate(models) {}
}

export default Employee;
Employee.prototype.authenticate = async (plainPassword, password) => {
  const match = await bcrypt.compare(plainPassword, password);
  if (match) {
    return true;
  }
  return false;
};
