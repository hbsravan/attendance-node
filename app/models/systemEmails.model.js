import Sequelize from 'sequelize';

class SystemEmail extends Sequelize.Model {
  static init(sequelize, DataTypes) {
    return super.init({
      title: {
        type: DataTypes.STRING(250),
        defaultValue: '',
      },
      code: {
        type: DataTypes.STRING(250),
        unique: true,
        validate: {
          isUnique(value, next) {
            const self = this;
            SystemEmail.findOne({ where: { code: value } })
              .then(user => {
                if (user && self.id !== user.id) {
                  return next('The code you entered is already in our system.');
                }
                return next();
              })
              .catch(err => next(err));
          },
        },
      },
      subject: {
        type: DataTypes.STRING(500),
      },
      message: {
        type: DataTypes.TEXT,
      },
    }, {
      timestamps: true,
      tableName: 'systemEmails',
      sequelize,
    });
  }
}


export default SystemEmail;
