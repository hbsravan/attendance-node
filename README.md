# Node Mysql HB Attendance

Node Mysql HB Attendance with Express js.

# Table of Contents

- [File Structure](#file-structure)
- [Getting Started](#getting-started)
  - [Dependencies](#dependencies)
  - [Installing](#installing)
- [Configuration](#configuration)
- [Seeding Database](#seeding-database)
- [Environment Configuration](#environment-configuration)

## File Structure

Here's how our folder strcture looks for api:

```
Node Mysql HB Attendance/
 ├──app/                           * source files
 |   ├──admin/                     * source files for admin api
 │       ├──controllers/           * controllers source files for admin api
 |       ├──services/              * service files for admin api
 |       ├──_apidoc.js             * common api doc variable used in admin api documentation
 |       ├──apidoc.json            * api doc configuration for admin api documentation
 |       ├──header.md              * api doc header file for admin api documentation
 |       ├──routes.js              * routes configuration for admin api
 |   ├──models/                    * contains the mongoose model files
 |   ├──public/                    * static assets are served from here
 |   ├──services/                  * source files for common functionality
 |   ├──utils/                     * source files for common functionality
 |   ├──moble/                     * source files for mobile api
 │       ├──controllers/           * controllers source files for website api
 |       ├──services/              * service files for web api
 |       ├──_apidoc.js             * common api doc variable used in website api documentation
 |       ├──apidoc.json            * api doc configuration for website api documentation
 |       ├──header.md              * api doc header file for website api documentation
 |       ├──routes.js              * routes configuration for website api
 |   ├──routes.js                  * application routes configuration
 |   ├──config.js                  * application configuration files
 ├──.eslintrc.json                 * javascript lint config
 ├──.gitignore                     * gitignore configuration
 ├──gulpfile.js                    * gulp task configuration file
 ├──index.js                       * Index.html: where we start express server
 ├──nodemon.json                   * nodemon configuration file
 ├──package.json                   * what npm uses to manage it's dependencies
```

# Getting Started

## Dependencies

What you need to run this app:

- `node` and `npm` (`brew install node`)
- Ensure you're running the latest versions Node `v8.x.x`+ (or `v9.x.x`) and NPM `5.x.x`+

Once you have those, you should install these globals with `npm install --global`:

- `gulp` (`npm install --global gulp`)
- `eslint` (`npm install --global eslint`)
- `prettier` (`npm install --global prettier`)
- `prettier-eslint` (`npm install --global prettier-eslint`)

## Installing

- `npm install` to install all dependencies or `yarn`
- `npm start` to start the server

# Configuration

Configuration files live in `/config` for different environments of your application

## Other commands

### build api documentation

```bash
# create api documentation for mobile api
gulp apidoc --type mobile

# create api documentation for admin api
gulp apidoc --type admin

# create api documentation for website api
gulp apidoc --type web
```

# Seeding Database

Use below commands for seeding the database with dummy values.

1. Set `DB_CONN_URL` variable value in `.env` file

2. First Run the below command to geneate seed server file

```bash
npm run webpack:seed
```

3. Use below arguments to run the seed server with command `npm run serve:seed` as explained below

| Type         | Params         | Description                                |
| ------------ | -------------- | ------------------------------------------ |
| email        | --email        | Use this argument to run email seed        |
| notification | --notification | Use this argument to run notification seed |
| page         | --page         | Use this argument to run page seed         |
| region       | --region       | Use this argument to run region seed       |
| setting      | --setting      | Use this argument to run setting seed      |
| user         | --user         | Use this argument to run user seed         |

```bash
# To run service and page seed
npm run serve:seed -- --page --service

# To run email and region seed
npm run serve:seed -- --email --region
```

# Environment Configuration

Environment Configuration variable needs to be set as per file [.env.example](./.env.example)
